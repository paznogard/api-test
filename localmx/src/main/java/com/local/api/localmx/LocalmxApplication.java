package com.local.api.localmx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class LocalmxApplication {

	public static void main(String[] args) {
		SpringApplication.run(LocalmxApplication.class, args);
	
	}
	}
