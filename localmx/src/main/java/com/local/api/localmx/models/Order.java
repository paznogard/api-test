package com.local.api.localmx.models;
import java.util.Date;
import java.util.List; 
public class Order{
    public String instructions;
    public List<Object> coupons;
    public List<Object> tax_list;
    public Object missed_reason;
    public Object billing_details;
    public Object fulfillment_option;
    public Object table_number;
    public int id;
    public double total_price;
    public double sub_total_price;
    public int tax_value;
    public int persons;
    public String latitude;
    public String longitude;
    public String client_first_name;
    public String client_last_name;
    public String client_email;
    public String client_phone;
    public String restaurant_name;
    public String currency;
    public String type;
    public String status;
    public String source;
    public boolean pin_skipped;
    public Date accepted_at;
    public String tax_type;
    public String tax_name;
    public Date fulfill_at;
    public String client_language;
    public Object integration_payment_provider;
    public int integration_payment_amount;
    public Object reference;
    public int restaurant_id;
    public int client_id;
    public Date updated_at;
    public String restaurant_phone;
    public String restaurant_timezone;
    public Object card_type;
    public List<String> used_payment_methods;
    public int company_account_id;
    public int pos_system_id;
    public String restaurant_key;
    public String restaurant_country;
    public String restaurant_city;
    public String restaurant_state;
    public String restaurant_zipcode;
    public String restaurant_street;
    public String restaurant_latitude;
    public String restaurant_longitude;
    public boolean client_marketing_consent;
    public String restaurant_token;
    public Object gateway_transaction_id;
    public Object gateway_type;
    public int api_version;
    public String payment;
    public boolean for_later;
    public String client_address;
    public ClientAddressParts client_address_parts;
    public List<Item> items;


    public String getInstructions() {
        return this.instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public List<Object> getCoupons() {
        return this.coupons;
    }

    public void setCoupons(List<Object> coupons) {
        this.coupons = coupons;
    }

    public List<Object> getTax_list() {
        return this.tax_list;
    }

    public void setTax_list(List<Object> tax_list) {
        this.tax_list = tax_list;
    }

    public Object getMissed_reason() {
        return this.missed_reason;
    }

    public void setMissed_reason(Object missed_reason) {
        this.missed_reason = missed_reason;
    }

    public Object getBilling_details() {
        return this.billing_details;
    }

    public void setBilling_details(Object billing_details) {
        this.billing_details = billing_details;
    }

    public Object getFulfillment_option() {
        return this.fulfillment_option;
    }

    public void setFulfillment_option(Object fulfillment_option) {
        this.fulfillment_option = fulfillment_option;
    }

    public Object getTable_number() {
        return this.table_number;
    }

    public void setTable_number(Object table_number) {
        this.table_number = table_number;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getTotal_price() {
        return this.total_price;
    }

    public void setTotal_price(double total_price) {
        this.total_price = total_price;
    }

    public double getSub_total_price() {
        return this.sub_total_price;
    }

    public void setSub_total_price(double sub_total_price) {
        this.sub_total_price = sub_total_price;
    }

    public int getTax_value() {
        return this.tax_value;
    }

    public void setTax_value(int tax_value) {
        this.tax_value = tax_value;
    }

    public int getPersons() {
        return this.persons;
    }

    public void setPersons(int persons) {
        this.persons = persons;
    }

    public String getLatitude() {
        return this.latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return this.longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getClient_first_name() {
        return this.client_first_name;
    }

    public void setClient_first_name(String client_first_name) {
        this.client_first_name = client_first_name;
    }

    public String getClient_last_name() {
        return this.client_last_name;
    }

    public void setClient_last_name(String client_last_name) {
        this.client_last_name = client_last_name;
    }

    public String getClient_email() {
        return this.client_email;
    }

    public void setClient_email(String client_email) {
        this.client_email = client_email;
    }

    public String getClient_phone() {
        return this.client_phone;
    }

    public void setClient_phone(String client_phone) {
        this.client_phone = client_phone;
    }

    public String getRestaurant_name() {
        return this.restaurant_name;
    }

    public void setRestaurant_name(String restaurant_name) {
        this.restaurant_name = restaurant_name;
    }

    public String getCurrency() {
        return this.currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSource() {
        return this.source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public boolean isPin_skipped() {
        return this.pin_skipped;
    }

    public boolean getPin_skipped() {
        return this.pin_skipped;
    }

    public void setPin_skipped(boolean pin_skipped) {
        this.pin_skipped = pin_skipped;
    }

    public Date getAccepted_at() {
        return this.accepted_at;
    }

    public void setAccepted_at(Date accepted_at) {
        this.accepted_at = accepted_at;
    }

    public String getTax_type() {
        return this.tax_type;
    }

    public void setTax_type(String tax_type) {
        this.tax_type = tax_type;
    }

    public String getTax_name() {
        return this.tax_name;
    }

    public void setTax_name(String tax_name) {
        this.tax_name = tax_name;
    }

    public Date getFulfill_at() {
        return this.fulfill_at;
    }

    public void setFulfill_at(Date fulfill_at) {
        this.fulfill_at = fulfill_at;
    }

    public String getClient_language() {
        return this.client_language;
    }

    public void setClient_language(String client_language) {
        this.client_language = client_language;
    }

    public Object getIntegration_payment_provider() {
        return this.integration_payment_provider;
    }

    public void setIntegration_payment_provider(Object integration_payment_provider) {
        this.integration_payment_provider = integration_payment_provider;
    }

    public int getIntegration_payment_amount() {
        return this.integration_payment_amount;
    }

    public void setIntegration_payment_amount(int integration_payment_amount) {
        this.integration_payment_amount = integration_payment_amount;
    }

    public Object getReference() {
        return this.reference;
    }

    public void setReference(Object reference) {
        this.reference = reference;
    }

    public int getRestaurant_id() {
        return this.restaurant_id;
    }

    public void setRestaurant_id(int restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public int getClient_id() {
        return this.client_id;
    }

    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }

    public Date getUpdated_at() {
        return this.updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public String getRestaurant_phone() {
        return this.restaurant_phone;
    }

    public void setRestaurant_phone(String restaurant_phone) {
        this.restaurant_phone = restaurant_phone;
    }

    public String getRestaurant_timezone() {
        return this.restaurant_timezone;
    }

    public void setRestaurant_timezone(String restaurant_timezone) {
        this.restaurant_timezone = restaurant_timezone;
    }

    public Object getCard_type() {
        return this.card_type;
    }

    public void setCard_type(Object card_type) {
        this.card_type = card_type;
    }

    public List<String> getUsed_payment_methods() {
        return this.used_payment_methods;
    }

    public void setUsed_payment_methods(List<String> used_payment_methods) {
        this.used_payment_methods = used_payment_methods;
    }

    public int getCompany_account_id() {
        return this.company_account_id;
    }

    public void setCompany_account_id(int company_account_id) {
        this.company_account_id = company_account_id;
    }

    public int getPos_system_id() {
        return this.pos_system_id;
    }

    public void setPos_system_id(int pos_system_id) {
        this.pos_system_id = pos_system_id;
    }

    public String getRestaurant_key() {
        return this.restaurant_key;
    }

    public void setRestaurant_key(String restaurant_key) {
        this.restaurant_key = restaurant_key;
    }

    public String getRestaurant_country() {
        return this.restaurant_country;
    }

    public void setRestaurant_country(String restaurant_country) {
        this.restaurant_country = restaurant_country;
    }

    public String getRestaurant_city() {
        return this.restaurant_city;
    }

    public void setRestaurant_city(String restaurant_city) {
        this.restaurant_city = restaurant_city;
    }

    public String getRestaurant_state() {
        return this.restaurant_state;
    }

    public void setRestaurant_state(String restaurant_state) {
        this.restaurant_state = restaurant_state;
    }

    public String getRestaurant_zipcode() {
        return this.restaurant_zipcode;
    }

    public void setRestaurant_zipcode(String restaurant_zipcode) {
        this.restaurant_zipcode = restaurant_zipcode;
    }

    public String getRestaurant_street() {
        return this.restaurant_street;
    }

    public void setRestaurant_street(String restaurant_street) {
        this.restaurant_street = restaurant_street;
    }

    public String getRestaurant_latitude() {
        return this.restaurant_latitude;
    }

    public void setRestaurant_latitude(String restaurant_latitude) {
        this.restaurant_latitude = restaurant_latitude;
    }

    public String getRestaurant_longitude() {
        return this.restaurant_longitude;
    }

    public void setRestaurant_longitude(String restaurant_longitude) {
        this.restaurant_longitude = restaurant_longitude;
    }

    public boolean isClient_marketing_consent() {
        return this.client_marketing_consent;
    }

    public boolean getClient_marketing_consent() {
        return this.client_marketing_consent;
    }

    public void setClient_marketing_consent(boolean client_marketing_consent) {
        this.client_marketing_consent = client_marketing_consent;
    }

    public String getRestaurant_token() {
        return this.restaurant_token;
    }

    public void setRestaurant_token(String restaurant_token) {
        this.restaurant_token = restaurant_token;
    }

    public Object getGateway_transaction_id() {
        return this.gateway_transaction_id;
    }

    public void setGateway_transaction_id(Object gateway_transaction_id) {
        this.gateway_transaction_id = gateway_transaction_id;
    }

    public Object getGateway_type() {
        return this.gateway_type;
    }

    public void setGateway_type(Object gateway_type) {
        this.gateway_type = gateway_type;
    }

    public int getApi_version() {
        return this.api_version;
    }

    public void setApi_version(int api_version) {
        this.api_version = api_version;
    }

    public String getPayment() {
        return this.payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public boolean isFor_later() {
        return this.for_later;
    }

    public boolean getFor_later() {
        return this.for_later;
    }

    public void setFor_later(boolean for_later) {
        this.for_later = for_later;
    }

    public String getClient_address() {
        return this.client_address;
    }

    public void setClient_address(String client_address) {
        this.client_address = client_address;
    }

    public ClientAddressParts getClient_address_parts() {
        return this.client_address_parts;
    }

    public void setClient_address_parts(ClientAddressParts client_address_parts) {
        this.client_address_parts = client_address_parts;
    }

    public List<Item> getItems() {
        return this.items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "{" +
            " instructions='" + getInstructions() + "'" +
            ", coupons='" + getCoupons() + "'" +
            ", tax_list='" + getTax_list() + "'" +
            ", missed_reason='" + getMissed_reason() + "'" +
            ", billing_details='" + getBilling_details() + "'" +
            ", fulfillment_option='" + getFulfillment_option() + "'" +
            ", table_number='" + getTable_number() + "'" +
            ", id='" + getId() + "'" +
            ", total_price='" + getTotal_price() + "'" +
            ", sub_total_price='" + getSub_total_price() + "'" +
            ", tax_value='" + getTax_value() + "'" +
            ", persons='" + getPersons() + "'" +
            ", latitude='" + getLatitude() + "'" +
            ", longitude='" + getLongitude() + "'" +
            ", client_first_name='" + getClient_first_name() + "'" +
            ", client_last_name='" + getClient_last_name() + "'" +
            ", client_email='" + getClient_email() + "'" +
            ", client_phone='" + getClient_phone() + "'" +
            ", restaurant_name='" + getRestaurant_name() + "'" +
            ", currency='" + getCurrency() + "'" +
            ", type='" + getType() + "'" +
            ", status='" + getStatus() + "'" +
            ", source='" + getSource() + "'" +
            ", pin_skipped='" + isPin_skipped() + "'" +
            ", accepted_at='" + getAccepted_at() + "'" +
            ", tax_type='" + getTax_type() + "'" +
            ", tax_name='" + getTax_name() + "'" +
            ", fulfill_at='" + getFulfill_at() + "'" +
            ", client_language='" + getClient_language() + "'" +
            ", integration_payment_provider='" + getIntegration_payment_provider() + "'" +
            ", integration_payment_amount='" + getIntegration_payment_amount() + "'" +
            ", reference='" + getReference() + "'" +
            ", restaurant_id='" + getRestaurant_id() + "'" +
            ", client_id='" + getClient_id() + "'" +
            ", updated_at='" + getUpdated_at() + "'" +
            ", restaurant_phone='" + getRestaurant_phone() + "'" +
            ", restaurant_timezone='" + getRestaurant_timezone() + "'" +
            ", card_type='" + getCard_type() + "'" +
            ", used_payment_methods='" + getUsed_payment_methods() + "'" +
            ", company_account_id='" + getCompany_account_id() + "'" +
            ", pos_system_id='" + getPos_system_id() + "'" +
            ", restaurant_key='" + getRestaurant_key() + "'" +
            ", restaurant_country='" + getRestaurant_country() + "'" +
            ", restaurant_city='" + getRestaurant_city() + "'" +
            ", restaurant_state='" + getRestaurant_state() + "'" +
            ", restaurant_zipcode='" + getRestaurant_zipcode() + "'" +
            ", restaurant_street='" + getRestaurant_street() + "'" +
            ", restaurant_latitude='" + getRestaurant_latitude() + "'" +
            ", restaurant_longitude='" + getRestaurant_longitude() + "'" +
            ", client_marketing_consent='" + isClient_marketing_consent() + "'" +
            ", restaurant_token='" + getRestaurant_token() + "'" +
            ", gateway_transaction_id='" + getGateway_transaction_id() + "'" +
            ", gateway_type='" + getGateway_type() + "'" +
            ", api_version='" + getApi_version() + "'" +
            ", payment='" + getPayment() + "'" +
            ", for_later='" + isFor_later() + "'" +
            ", client_address='" + getClient_address() + "'" +
            ", client_address_parts='" + getClient_address_parts() + "'" +
            ", items='" + getItems() + "'" +
            "}";
    }

}
